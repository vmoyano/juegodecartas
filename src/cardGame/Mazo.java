package cardGame;

import java.util.Random;

public class Mazo {
	private Carta[] cartas;
	private int cantCartas;
	
	//IREP invariante de representacion
	//cartas tiene largo 40
	//en el arreglo cartas no hay repetidos
//    las primeras cantCartas son no nulas	
	//cartas.length == 40
	//cartas[i] != cartas[j] (para todo i != j entre 0 y 40)

	public Mazo() {
		this.cartas = new Carta[40];
		this.cantCartas = 40;
		int j = 0;
		for (int p = 0; p <= 3; p++) {
			for (int i = 1; i <= 7; i++) {
				cartas[j] = new Carta(p, i);
				j++;
			}
			for (int i = 10; i <= 12; i++) {
				cartas[j] = new Carta(p, i);
				j++;
			}
		}
	}

	public void mostrar() {
		for (int i = 0; i < cantCartas; i++) {
			System.out.println(cartas[i]);
		}
	}
	
	public void mezclar() {
		Random r = new Random();
		for (int k = 0; k < 400; k++) {
			int pos1 = r.nextInt(cantCartas);
			int pos2 = r.nextInt(cantCartas);

			Carta aux = cartas[pos1];
			cartas[pos1] = cartas[pos2];
			cartas[pos2] = aux;
		}
	}
	
	public Carta darCarta() {
		Carta c = cartas[cantCartas-1];
		cartas[cantCartas-1] = null;
		cantCartas--;
		return c;
	}

	public int getCantCartas() {
		return cantCartas;
	}

	 
	
	
}
