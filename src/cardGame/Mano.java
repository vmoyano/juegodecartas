package cardGame;

public class Mano {
//	variables de instancia
	private int cantCartas;
	private Carta[] cartas;
	
	public Mano() {
		this.cantCartas = 0;
		this.cartas = new Carta[40];
	}
	
	public void mostrar(){
		for(int i = 0 ; i<cantCartas; i++) {
			System.out.println(cartas[i]);
		}
	}
	
	public void recibirCarta(Carta c) {
		this.cartas[cantCartas] = c;
		this.cantCartas++;
	}
	
	public Carta jugarCarta(int k) {
		Carta c = cartas[k];
		//reacomodar
//		for(int i=k; i<cartas.length-1;i++) {
//			cartas[i]=cartas[i+1];
//		}
		cartas[k] = cartas[cantCartas-1];
		cartas[cantCartas-1] = null;
		cantCartas--;
		return c;
	}
	
	public int cantidadDeCartas() {
		return cantCartas;
	}
	
	
	
	
	
	

}
