package cardGame;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Mazo m = new Mazo();
		Mano jugador = new Mano();
		Carta mesa;
		Scanner scan = new Scanner(System.in);

		m.mezclar();
//		m.mostrar();
		mesa = m.darCarta();

//		Carta c = m.darCarta();
		jugador.recibirCarta(m.darCarta());
		jugador.recibirCarta(m.darCarta());

		while (jugador.cantidadDeCartas() > 0 && m.getCantCartas() > 0) {
			System.out.println("Carta en la mesa: " + mesa);
			jugador.mostrar();

			System.out.println("Elegir una carta o 0 para tomar una carta del mazo..");
			int k = scan.nextInt();

			if (k == 0 || k>=jugador.cantidadDeCartas()) {
				jugador.recibirCarta(m.darCarta());
			} else {
				Carta c = jugador.jugarCarta(k - 1);
				if (mesa.compatible(c)) {
					mesa = c;
					System.out.println("Mesa: " + mesa);
				} else {
					jugador.recibirCarta(c);
					jugador.recibirCarta(m.darCarta());
					System.out.println("Carta de penalizacion!");
				}
			}
		}
		if(jugador.cantidadDeCartas()==0) {
			System.out.println("GANASTE!!!!");
		} else {
			System.out.println("PERDISTE!! JODETE!!");
		}
	}

}
