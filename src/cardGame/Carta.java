package cardGame;

public class Carta {
	private int palo;
	private int numero;
	
	//IREP invariante de prepresentacion
	// 1 <= numero <= 7 || 10 <= numero <= 12
	// 0 <= palo <= 3

	public Carta(int palo, int numero) {
		this.palo = palo;
		this.numero = numero;
	}

//	1,1 ---> 1 de espada
	public String toString() {
		String[] nombreDePalo = { "espada", "oro", "basto", "copas" };
		return this.numero + " de " + nombreDePalo[this.palo];
	}

	public boolean compatible(Carta c) {
		return this.numero==c.numero || this.palo==c.palo;
	}

}
